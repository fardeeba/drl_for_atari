import torch
import os
from model import Linear_QNet
from agent import Agent
from snake_game import SnakeGameAI, Direction, Point


model = Linear_QNet(11,256,3)
model_path = 'D:\FAST_MSDS\DL\DRL\Snake_Game\python-fun\snake-pygame\model\model.pth'
total_score = 0
agent = Agent()
game = SnakeGameAI()

model_state_dict = torch.load(model_path)
model.load_state_dict(model_state_dict)

done = False

while not done:

    old_state = agent.get_State(game)
    state = torch.tensor(old_state, dtype=torch.float)
    final_move = [0,0,0]
    prediction = model(state) #this can be raw value, we pick index of max value and set it to one
    move = torch.argmax(prediction).item()
    final_move[move] = 1

    #perform move and get new state
    reward, done, score = game.play_step(final_move)
    new_state = agent.get_State(game)

    total_score += score

print("Game over your score is ",score,"!")