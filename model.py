import torch
import torch.nn.functional as F
import os

class Linear_QNet(torch.nn.Module):
    def __init__(self,input_size,hidden_size,output_size):
        super().__init__()
        self.linear1 = torch.nn.Linear(input_size,hidden_size)
        self.linear2 = torch.nn.Linear(hidden_size,520)
        self.linear3 = torch.nn.Linear(520,output_size)
        self.relu = torch.nn.ReLU()

    def forward(self,x):
        x = self.linear1(x)
        x = self.relu(x)
        x = self.linear2(x)
        x = self.relu(x)
        x = self.linear3(x)
        return x
    
    def save(self,file_name='model.pth'):
        model_folder_path = "./model"
        if not os.path.exists(model_folder_path):
            os.makedirs(model_folder_path)
        
        file_name = os.path.join(model_folder_path,file_name)
        torch.save(self.state_dict(), file_name)


class QTrainer:
    def __init__(self,model,lr,gamma):
        self.lr = lr
        self.model = model
        self.gamma = gamma
        self.optimizer = torch.optim.Adam(model.parameters(), lr=self.lr)
        self.loss = torch.nn.MSELoss()

    def train_step(self,state,action,reward,next_state,done):
        state = torch.tensor(state, dtype=torch.float)
        action = torch.tensor(action, dtype=torch.float)
        reward = torch.tensor(reward, dtype=torch.float)
        next_state = torch.tensor(next_state, dtype=torch.float)

        #if we had called train_step function from train_long_memory funtion it will already contain a batch for all these i.e. (n,x)

        #if we had called it from train_short_memory it contains only one value for all so we need to convert into batch format i.e (1,x)

        if len(state.shape) == 1:
            state = torch.unsqueeze(state,0)
            action = torch.unsqueeze(action,0)
            reward = torch.unsqueeze(reward,0)
            next_state = torch.unsqueeze(next_state,0)
            done = (done, ) #tuple with one value

        # 1: Get predicted Q values with current state

        pred = self.model(state) #pred has 3 values since we have 3 possible actions

        # Qnew = Reward + gamma * max(next predicted Q value) -> only do this if not done
        # replace max value in action list with Q new

        target = pred.clone()
        for idx in range(len(state)):
            Q_new = reward[idx]
            if not done[idx]:
                Q_new = reward[idx] + self.gamma * torch.max(self.model(next_state[idx]))

            target[idx][torch.argmax(action).item()] = Q_new

        self.optimizer.zero_grad()
        loss_val = self.loss(target,pred)
        loss_val.backward()
        self.optimizer.step()
